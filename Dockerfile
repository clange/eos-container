FROM --platform=linux/amd64 gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

RUN dnf install -y dnf-plugins-core epel-release && \
    dnf config-manager --add-repo "https://storage-ci.web.cern.ch/storage-ci/eos/diopside/tag/testing/el-9s/x86_64/" && \
    dnf config-manager --add-repo "https://storage-ci.web.cern.ch/storage-ci/eos/diopside-depend/el-9s/x86_64/" && \
    dnf install -y --nogpgcheck eos-client eos-fusex jemalloc-devel \
    python3-root python3-xrootd \
    xrootd xrootd-client xrootd-fuse \
    xrootd-voms && \
    yum -y clean all && \
    rm -rf /var/cache

RUN mkdir -p /eos

ENV EOS_MGM_URL=root://eoscms.cern.ch

CMD ["bin/bash"]
